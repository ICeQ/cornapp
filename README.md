# Compact Open Remote Nao (CORN)

## O projekcie

Zadaniem aplikacji będzie dostarczenie użytkownikowi możliwości oglądania na ekranie tego, co widzi robot przez swoją kamerkę, jak również wydawania mu komend zdalnie, siedząc przy komputerze, dodatkowo w języku polskim. Aplikacja będzie napisana w modelu komponentowym, co pozwoli na proste dokładanie kolejnych funkcjonalności.

## Technologie

* .Net
* Google speech2text
* Microsoft Translator
* NAOqi.NET

## Zaplanowane funkcje

* Interfejs użytkownika
* Integracja komponentów
* Komponenty:
    * Komponent odpowiedzialny za przetwarzanie poleceń głosowych w języku polskim na komendy zrozumiałe przez robota
    * Komponent umożliwiający śledzenie obrazu z kamery robota

## Etapy prac

### Spotkanie 1

Dyskusja nt. dostępnych tematów, budowanie zespołu.

### Spotkanie 2

Przegląd pokrewnych rozwiązań, określenie używanych technologii oraz wstępnych wymagań dla projektu.

### Spotkanie 3

Przygotowanie wizji projektu i podział prac.

### Spotkanie 4

Sprawdzenie możliwości technologii, przygotowanie dema komponentu oraz mockupu interfejsu graficznego.

### Spotkanie 5

Przedstawienie działającego interfejsu graficznego oraz komponentu zmieniającego mowę na tekst, przedstawienie problemów z wykorzystanymi technologiami (urzeczywistnienie się jednego z ryzyk zawartych w wizji).

### Spotkanie 6

Przygotowanie wspólnego repozytorium, ustalenie interfejsów łączenia komponentów z aplikacją, przygotowanie raportu z postępów prac.

### Spotkanie 7

Implementacja i pierwsze testy aplikacji z rzeczywistym robotem.

### Spotkanie 8

Poprawa znalezionych błędów, powtórne testy z rzeczywistym robotem, przygotowanie podsumowania projektu oraz dokumentacji użytkownika, oddanie projektu.

## Dokumenty

* [Wizja projektu](http://student.agh.edu.pl/~kluba/toik/TOIK_Wizja.pdf)
* [Dokumentacja użytkownika](http://student.agh.edu.pl/~megaweb/TOiK/DokumentacjaUzytkownika.pdf)
* [Podsumowanie projektu](http://student.agh.edu.pl/~megaweb/TOiK/PodsumowanieProjektu.pdf)

## Bibliografia

* https://community.aldebaran-robotics.com/
* http://totologic.blogspot.com/2013/09/nao-new-c-library-with-event.html