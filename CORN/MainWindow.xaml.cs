﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CORN
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        class ComponentContainer
        {
            public String ComponentName { get; set; }
            public Components.Component component { get; set; }
            public ComponentContainer(Components.Component component)
            {
                this.component = component;
                this.ComponentName = component.GetName();
                Console.WriteLine(this.ComponentName);
            }
        }

        private App app;
        public String IPAddress {get; set;}
        public int port { get; set; }
        private String componentToAdd;
        private Grid gridToChange;
        NaoConnector naoConnector = new NaoConnector();
        List<ComponentContainer> components = new List<ComponentContainer>();

        public MainWindow()
        {
            this.IPAddress = "127.0.0.1";
            this.port = 9559;
            this.app = (App)Application.Current;
            foreach(Components.Component c in this.app.GetComponents())
            {
                ComponentContainer cc = new ComponentContainer(c);
                components.Add(cc);
            }
            InitializeComponent();

            componentButtons.DataContext = components;
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            naoConnector.closeLeftHand();
            String[] temp = IPAddressTextBox.Text.Split(':');
            try
            {
                IPAddress = temp[0];
                port = int.Parse(temp[1]);
                statusImage.Source = new BitmapImage(new Uri(@"/Resources/Images/ok_mark.png", UriKind.Relative));
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
                statusImage.Source = new BitmapImage(new Uri(@"/Resources/Images/not_ok_mark.png", UriKind.Relative));
            }
            Console.WriteLine(IPAddress + " " + port);
        }

        private void AddComponentButton_Click(object sender, RoutedEventArgs e)
        {
            //determining which button should be replaced
            Button but = (Button)sender;
            gridToChange = (Grid)but.Parent;

            popup.IsOpen = true;
        }

        private void radioButtonChecked(object sender, RoutedEventArgs e)
        {
            //determining which component should be put instead of button
            RadioButton rb = (RadioButton)sender;
            componentToAdd = rb.Content.ToString();
        }

        private void AddComponentButtonClicked(object sender, RoutedEventArgs e)
        {
            popup.IsOpen = false;

            PaintComponent();
        }

        private void PaintComponent()
        {
            UIElement gui = new UIElement();

            foreach (ComponentContainer cc in components)
            {
                if (cc.component.GetName().Equals(componentToAdd))
                {
                    gui = cc.component.GetContent();
                    cc.component.Start(IPAddress, port);
                }
            }

            //Saving old content of grid (button)
            UIElement oldButton = null;
            foreach (var i in gridToChange.Children) {
                oldButton = (UIElement) i;
            }

            gridToChange.Children.Clear();
            try
            {
                gridToChange.Children.Add(gui);
            }
            catch (InvalidOperationException)
            {
                //thrown when component was already added
                gridToChange.Children.Add(oldButton);
            }
        }
    }
}
