﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Reflection;
using System.IO;
using CORN.Components;

namespace CORN
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        IList<Component> components;

        App()
        {
            this.components = new List<Component>();
            String componentsPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            String[] components = Directory.GetFiles(componentsPath+@"\components", "*.dll");

            foreach(String componentPath in components)
            {
                Assembly assembly = Assembly.LoadFile(componentPath);
                Type[] types = assembly.GetTypes();

                foreach(Type type in types)
                {
                    var constructor = type.GetConstructor(Type.EmptyTypes);
                    if (constructor != null)
                    {
                        object instance = Activator.CreateInstance(type);
                        if (instance != null && instance is Component)
                        {
                            Component component = (Component)instance;
                            this.components.Add(component);
                        }
                    }
                }
            }
        }

        ~App()
        {
            foreach (Component component in components)
            {
                component.Stop();
            }
        }

        public IList<Component> GetComponents()
        {
            return this.components;
        }
    }
}
