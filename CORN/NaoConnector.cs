﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using Aldebaran.Proxies;

namespace CORN
{
    class NaoConnector
    {
        public NaoConnector()
        {
            
        }

        public void saySomething() {
            try
            {
                TextToSpeechProxy tts = new TextToSpeechProxy("127.0.0.1", 9559);
                tts.say("Hello World from c sharp");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void closeRightHand()
        {
            try
            {
                MotionProxy mp = new MotionProxy("127.0.0.1", 9559);
                mp.closeHand("RHand");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        public void closeLeftHand()
        {
            try
            {
                MotionProxy mp = new MotionProxy("127.0.0.1", 9559);
                mp.openHand("LHand");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
