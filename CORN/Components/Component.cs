﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CORN.Components
{
    public interface Component
    {
        String GetName();
        UIElement GetContent();
        void Start(String ip, int port);
        void Stop();
    }
}
